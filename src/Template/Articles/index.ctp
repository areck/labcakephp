<h1>Articles</h1>

<?= $this->Html->link('Add Article', ['action' => 'add']) ?>

<table>
    <tr>
        <th>Title</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($articles as $article): ?>
    <tr>
        <td>
            <?= $this->Html->link(h($article->title), ['action' => 'view', h($article->slug)]) ?>
        </td>
        <td>
            <?= h($article->created->format(DATE_RFC850)) ?>
        </td>
        <td>
            <?= $this->Html->link('Edit', ['action' => 'edit', h($article->slug)]) ?>
            <?= $this->Form->postLink(
                'Delete',
                ['action' => 'delete', h($article->slug)],
                ['confirm' => 'Are you sure?'])
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
