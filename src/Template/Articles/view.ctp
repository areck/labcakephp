<!-- File: src/Template/Articles/view.ctp -->

<h1><?= h($article->title) ?></h1>
<p><?= h($article->body) ?></p>
<p><b>Tags:</b> <?= h($article->tag_string) ?></p>
<p><small>Created: <?= h($article->created->format(DATE_RFC850)) ?></small></p>
<p><?= $this->Html->link('Edit', ['action' => 'edit', h($article->slug)]) ?></p>

<table>
    <tr>
        <th>Comment</th>
        <th>Created</th>
        <th>Author</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($article->comments as $comment): ?>
    <tr>
        <td>
        <?= h($comment->comment) ?>
            
        </td>
        <td>
            <?= h($comment->created->format(DATE_RFC850)) ?>
        </td>
        <td>
        <?= $this->Html->link($comment->user->email, ['controller' => 'users','action' => 'view', h($comment->user_id)]) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

<h1>Add a comment</h1>
<?php
    echo $this->Form->create($newComment);
    // Hard code the user for now.
   
    echo $this->Form->control('comment');
    echo $this->Form->button(__('Save Comment'));
    echo $this->Form->end();
?>
