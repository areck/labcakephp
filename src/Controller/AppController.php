<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */


    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $this->loadComponent('Auth', [
             // Added this line
        'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);

        // Allow the display action so our PagesController
        // continues to work. Also enable the read only actions.
        $this->Auth->allow(['display', 'view', 'index']);



    }

    public function beforeRedirect($event, $url, $response){
        // Intercept la redirection seulement si on est authentifier ET la requete est en json
        // SI c'est angular qui cause un redirect
          if($this->request->is('json')){
            $authenticated = $this->Auth->user('id') != null;
            // Si je suis authentifier
            if($authenticated) {
                //TODO Il ne devrait pas y avoir de redirection dans les contrôleurs
                $this->setResponse($this->response->withStatus(500) );// Si un controlleur de l'api implémente un redirect on lève une erreur critique
            }else{
                //TODO Change le status code de la réponse pour un unauthorized et change le contenu de la requete selon la convention
                $this->setResponse($this->response->withStatus(403)->withType('application/json')->withStringBody(json_encode(['authenticated' => $authenticated])));
            }
          }
      }

    public function isAuthorized($user)
    {
        // By default deny access.
        return false;
    }
}
