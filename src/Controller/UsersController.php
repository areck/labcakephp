<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $userid = $this->Auth->user('id');
        $user = $this->Users->get($userid);

        $this->set(compact('user'));
    }

    public function initialize()
    {
        parent::initialize();
       // Add the 'add' action to the allowed actions list.
        $this->Auth->allow(['logout', 'add']);
    }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      /*  $user = $this->Users->get($id, [
            'contain' => ['Articles'],
        ]);

        $this->set('user', $user);*/

        $userid = $this->Auth->user('id');
        $user = $this->Users->get($userid);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']); 
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            $img = $this->request->data['image'];
            if($img['name'] != '')
            {
            $imgName = $img['name'];
            $imgType = $img['type'];
            $imgTmp = $img['tmp_name'];
            $imgSize = $img['size'];

            $handle = fopen($imgTmp, "rb");
            $content = fread($handle, $imgSize);
            fclose($handle);
            $encode = base64_encode($content);

            $user->image = $encode;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->getData());

           
            $img = $this->request->data['image'];

            if($img['name'] != '')
            {
            $imgName = $img['name'];
            $imgType = $img['type'];
            $imgTmp = $img['tmp_name'];
            $imgSize = $img['size'];

            $handle = fopen($imgTmp, "rb");
            $content = fread($handle, $imgSize);
            fclose($handle);
            $encode = base64_encode($content);

            $user->image = $encode;

            }

           


            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'articles', 'action' => 'index']);
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        $user_id = $this->request->getParam('pass.0');
        
        if(in_array($action, ['view','edit','delete']))
        {
            if($user['id'] == $user_id)
            {
                return true;
            }
            else
            {
               
                return false;
            }
            
        }

        

    }
}
