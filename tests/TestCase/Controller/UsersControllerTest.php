<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\UsersController Test Case
 *
 * @uses \App\Controller\UsersController
 */
class UsersControllerTest extends TestCase
{

    private $img = '\/9j\/4AAQSkZJRgABAQAAAQABAAD\/2wCEAAkGBxATEhUSEg8VFhUVEBcXFRUPFRYVFRcVFxIWFhUWFRUYHiggGBolHRUVITEhJSkrLi4uFx8zODMuNygtLisBCgoKDg0OGhAQGi0fHyItKzcvLi0uLS0tLSstKy0tLS0tLS8tLSstLS8rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf\/AABEIANkA6AMBIgACEQEDEQH\/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQUDBAYCBwj\/xABMEAACAQIDAwcGCwUFBwUAAAABAgADEQQhMQUSQQYTIlFhcYEHIzNzkbIUMkJSYnKCobHB8FNjwtHhNEOSosMkZHSTs9PxFRZUg5T\/xAAaAQACAwEBAAAAAAAAAAAAAAAAAwECBAUG\/8QAJBEBAAICAQMFAQEBAAAAAAAAAAECAxExBBIhEzJBUWEiBRT\/2gAMAwEAAhEDEQA\/APpERE1sRJkRAEREAREQBJkRAESZEAREQBJiRAESGYDUgd+Ulc8xn3ZwBERAEREAGIiAIiIAl9sr0NP1a+6JQiX2yvQ0\/Vr7oisvB2HmWZKt3ZbfFCm\/Xe\/Z2RMVFvO1B1Kn8URJ6giImtiJIEAReCSREQQREQBJKnqM9UuPWBlPd+r8b37++RtMQwxJYZnvkSUERNOhVr1yfg9NObViprV2IUspswpIoJqAEEEkqLjInO1MmWmOO686hatZt4hh29tujhKfOVSbk2REzd2+ao\/EmwHEz5vtTlTjMQbGrzCE23MOxWw+lWFnY926OydRyj8n+0a9Vq\/wmhUa1kpsKlJUQfJQ9PU5kkZnsAtxuO2ZWwrAYik1JySEDAFWsDmlQEqx42vccR1Zo6qmSdUs0VxdvLRq4Cjm1VFdjcXcbzHqYs2Z+\/8AAHBRoqhui7jfOpdBv8S2MzVGJNzPMldc7N5WY6ha1bnVHyMVd8uyr8cHtJbune8neVmHxRCZ0q1vRVCOlbU030cewjiBPlM8st\/AgggkEEG4IIzBBzBGkvW8wpbHEvu8kAnQeycTyK5WGoVw2Ja9W3mqpsOdsL7rcBUAF\/pAHiDO74DuHYL8SY6LbjwzzWYnUsJETJU09lvG+X3ffMcsiSIiCEy92V6Gn6tfdEoZaYMdHDZX831\/QHtisvB2HmW5RU87UJBsVQA3yNt69hw1ETzQPnqvYtO+X1jrx1kRJ6iiImtiSZERAESZEARE1NqbTo4emalaoEXTPMk\/NRRmzdggG4DMGO2hTpLv1qyU1+dUZUHtNp802\/5QMQ\/Rw4NFTpYK+IYZZ53SmOHHUdITknSpUbnKjnePymJqVSL3sar3y7Bp1xU5I+Dq4p+X1fFcvMAl913qED+5ptY9zvuofBpUVfKjh\/kYZyep6lMHx3C9pwS4SmMyu8eBe7EeLXtM8r6ll4xVdPifKlU3W3cCAd02IrVWINsj\/Z532xOV+yqdCjSGNpIEoogFQ7lt1ALHftnlPjMkTL1GGM8RFp4MpEV4fofA7Tw9YXo16VQddGorj\/KTPW0dnUa9NqVamtSm2quLjrBHUQcwRmCMp+c2oqxDFRvA5NYXB7DqDLfZvKbH0Lc1jath8iseeU9nnbkDsUiYbf58x7LL97Pyv5ONga\/N3LUnBahUbWwPSpufnLcZ8QQdb2pLdc6DlJyzrYvDClXwqGolVHSrQJUXBKvem97dBnGTHMjKc5Srq+hzHA5EdhBzE34e\/s\/vlWfx6JiJIEah5dO0ixBBU2IIN1ZSNCCAQRPrXIvb7YrD3cjnaZ3KoAyLWutQA6BhY9h3hwnye\/8ATsl1yL2iaGMp3NkreZfPK5zot2nf6I9aZek6lTJXcPrZMiImhlIiIBIm9RxCKmH3gxtTX4oBHSsgvn1ntmjLnZ+HRqVIsisRTWxYAkZA5E6RHURbt\/nk7F8vOGxCmu46VyLC4AA3DY2N7nNvukzbTDIGLhFDHVgBc31ufCIindr+j3NxETcxEmREAREmAV23tsUsLRNWpnnuoi\/GdzfdRe3Im\/AAk5CfHNtbWr4irv1GBqkHdAvzdGmToo6su9iM8hla8tNtc\/iHbe8zQ3kpgaHdPnaluJLCw7FFtTKLC0yBdvjMbt2Hgo7AMoi9tzppx01G3qhSC6XJOrHMk9v8tJmtx+7qkaSLxZhEm0QBIiIAnoZ6\/wDmQBIJgAmY6tFW1GY0IyYdxEy698AQDWSoVIFQ3BNg+lzwDDgT16HsyE2CZFQAggi4ItY6WmCgxB5sm9hdSTmV0z7RkPEdcA2JDAkEA2IzVvmsM1bwNjJAkkwD7TsnHCvQpVgLc5SV7dRZQSPA3HhNucn5M8VvYQ073NHEVEz6ntWHh523hOsmqs7jbHaNToiIkoJfbK9DT9WvuiUIl9sr0NP1a+6IrLwdh5ltxERJ7loiJrYiJkprx7bZ6aXJkkAj+luF\/wAJG06YpVcqtoGhhK1VTZhT3UJ\/aORTp\/5mWWs4\/wAp9e2Fpp+0xaA9y06lUffTWFp1CaxuYfMK6AKlMaF1Udygtb2Jbxm3p3zG6XKn5pJ9qkfnPcytaSOMlUJvYaC5nm8yu3NAFhmzKLX4E9XfYWPXAJeiVFybNf4p6ra\/0mEmS7Em5kQBJAmGj8Z7\/R93+kjGMbCx+Wgy6ucW\/wB14BmJie6NPeZV62A9ptLZzTQHoqFG8BenzhIQ7pZzfIX6oBTSWa82doUQrCwtcG662YMVYA9Vx981YAmDFiw3\/mHe+zow7cr+Npng5i3CATcafrxkGYMGegtzcgWPeuR\/CbVOmW04CAdr5L3YPiaZFhu0Kg+1zqH7qazvp898nVYfCaygZcwM73vu1LD8fvn0GaMftZsnuIiJctMt9l4lRTpqeFJM+9R+vA9Up5ZbCwx3RUfPoIEuBkoW+XiT7L9Uz9RNvEV+zsXK3iIlD3LREma2JKvb9fhDPPMQ0nZOI8qR83hh+\/ZvZRZf453E4TypaYb69X3Vlb+2VsfuhwkTy7WBJ4DhmfADWbWMwVagwSvRek5XeC1LZre1wykqbZXscri9r55ttSVUJm2Zt0QNPH9ZWmhj2JUseDK3grAkewTO1+u\/V\/KYX3XvTHSYqRuICz2It8Rbn7oBmIgCb+zuT+0KiKwwFe5UXDqtLO2fpWWWi8g9ptnzNNeyrWA9wNKzesfK3bP05RD5x\/qIfvcflJxPyfWD8zOrHk32nvFv9lzUC3P1eBJ\/Y9s81\/J3tO2S4fJgRu12OhvbpUhqLjxkepX7T2W+nOKbG41Bmzitphiqmn0mbeazkIdyx3in1glxexlliuSO0k1wTkddJ6TgeAfePslDiaFSk7GvSqUsgq8+jUweJILgXuSB9mWi0TxKsxMcs9euzm7Hh+ZJ9pJPjMYELn\/Oejnp+u2Sh5MRAEAx7MpbwYA6VKl7+ta\/4zarVRbdXTt17rjX\/wAdU1cEehkdWc36952OftmSAdd5Mh\/tNXsw4++oP5T6PPnnktpnncU3AUqCjvLVifwWfRJox+1mye5EREuWS+2V6Gn6tfdEohL3ZXoafq190RWXg7DzLbiIiT3LRETWxEREATiPKmvmsM3+8Mv+Ki7f6c7ecz5RcOWwTMBc0qtOp3Df3HbwV2PhK39srU90Pl1VgoLHhn4DO8+18suToxtFUDhHSqHSoyl93IqwKhhcFScr6gHhPiGNQtTccSjD2qRP0Lga\/OU6dQfLpq3+JQfznOzzMamHSwxE7iXLbM8nWBp51d\/EN++ayd3NJZSPrb3fOpweDpUl3KVJKajRaSqi+xRM8ouUXK\/AYFkXFYgIzi6qFd23b23iEBIF75nqPVEbtb9P1Wq9iY8PXR1V0YMjqGVlNwykXBB4giZJVYiIgCQygixFwdQdJM5\/bHLXZ2FrLh6+KVKhtlZiF3vi77AEJftIyz0hETPCJmI5eNqch9n1rnmBSYm+\/hTzRv1lR0GP1lM53CeTQriENSuKuGUlmVgUqsQOgjbuTLfMkbvxbWsTPooMS8ZLR42rOOs\/D5X5Sdk4PDth1w+Hp0WfnWbmV3AUQItmUZHOopva\/R75xlVwATwAJ9gnY+VWvfGU0+ZhA3\/MquP9ITjXwtSrelTXeZlY2N7bqoXe9tOipHeQOM14t9sMuTXdLxhEIRQdQgB77ZzNr3zypvYjjpPRNvzMuo+h+TDDkUKz\/PxJA+qlNF97fnYyk5E4Tm8DQBFmanzjA6hqrGqQe0F7eEu5qrGoZLTuZIiJKpL7ZPoafq190Shl9sn0NP1a+6IrLwdh5ltxERJ7lpMiJrYiIkiAJhxeHSoj03F0dGRh1qwII9hmWIB8OxWEejUehU+PSfdYn5Q1Vx2MpVvG3CfYPJ9iQ+zsNY\/Ep80eu9FjS\/gv4yi5e8nWrKMRRW9amtmUa1aQJO6Pprcleu7DiLYvJHtIMtfD3vZlrJ1brjccDuZAT6yYOppqHQ6e+5fRJ888ovk1O0a6YiniRTYUxTcOpYFQxIZbHI9I5cctJ9DAk7v6P6ymSszE7hrtET4lobD2YuGw9LDoxZaVJUDNqbDU983oiVmUxBERBJPmPLLyT\/Dca2KXF7i1CpqqybzAqoU82b2zCjI6H2T6dBk1tNZ8K2rEx5Y6FIIqoNFUKL62AsLzJIEmQmHxryh1g20a30KdKmfCnzn+rOg8nGw91Diqi9Ksu7TBHxaF73+2bHuVO2V+zeTb47FVcXXTdw1Su1RFuCcQl7UjYaUyipe+baWtmfognVw01EbcvNk3MxD4TTohBzYPoyaZ+wxT+Ge6GENZ0oD+9qLTy1CswDkdy7x8Jn2tS3cRiF\/3uuf8VZ2\/inT+T3YwbECs2fNUi6qOBq3RGJP0RVy7ZSI86Xmf52+iAAZAWA0HZPU9Olv6zxNTKREQQkS+2V6Gn6tfdEoJfbJ9DT9WvuiKy8HYeZbcRESe5aIkzWxEiDEEkSZEEE5jH7GXDYobRodHd3vhVIC4ek3pXQDMOLByB8Y0xxJv08mVtWLRqVq2ms7hYIwOYNwRqM8joRNfaGPpUV36j2BNgFBZmY6KiAbzsbGyqCZUbFrDDuMI5shucKTkNwC5w9\/nILlRxQDXcYy8akpYMVBZb7rEC43rb1jwvYeycm9Zpbtl16Wi9e6FXz2Nq5pTTDpfI4jztUi5v5pGCplaxLsc81Gk9DY7m\/OY3EPfgrJSA7uZRW9pMtYlNrdqp\/8Ab9H9rif\/ANmK\/wC5JbYxHo8ZiU\/+xavt59XlrEO6U9sKrdxyHJqNdbjJw1CoBxO+u+rHs3VHbMmC2vTdhTZXpVSL81XAVyBqUIJWoBcXKM1uMsZgxeEp1V3aiBhvBgGGjKbqyngwOYIzENwjUs8qOUuIYUxRpkipiDzSldUUi9WrobbibxF8t7cHGWWLxKU0apUYKqi7E8B+Z7JSYGm7u2Jqruu67tNDrSo3uFP02Nma3Uq57gJdgxd9vyCc+Xsr+t2lTVQFUWVQAANAALADwnqInVcp8Z5R\/wBsxP8AxD\/kZ9J5BYfdwdN2ADVQHIN\/iWtS4fMCkjrYz59j8D8J2hUoL\/e42orEX6NNWPOm406CNY9dp9dVQAABYAWAGgA0EVSPMyde2oiGR24d33X\/AJzxEmNKRERBAJfbK9DT9WvuiUQl9sr0NP1a+6IrKdh+W1EREnuWiImtiIiIAiIgCIiAYMbhEqoUcZXBBBsysDdWVhmrAgEEaETDhdrvRPN4wgC4CYmwWnUubKKvClU0HzWJG6QTuDdhlBBBFwRYg5gjiCIrLhrkjybizWxz4WMTmk2caOWGxBo9VJgKlDQAWpMQUAt8WmyDM5TLsnauNqJvnC0mXnKiBqVZlZubrPT3hTdLAHcuOmcjOdlwWpy6OLPW7oIlO22MQMv\/AEvFHtWpgreF8QD909fD8Ww6GAKnqxVemg8TR52K0b3QtppbS2pSoAb7HeY2SmgLVHPUiDM9p0GpIGcqNuHHLReq2Jp0lXcLLh0uwQVF50mtVuCAm8bhFIte82sFs+lSJKL0m+O7kvUe2m9UYlmtfIE5cI\/Dg9T58EZuo9PxphSjVrMKuIAUKb0qCneVDweowyer3dFeFz0jvwRE6VKRWNQ5trzadySREiWVcH5OsBv18XjCMjia9Okexq7VKrDrGdNfsNO8mjsPZi4ahToKbhFsWIsWYks7kcCzEnxm9IrGoWtO5IiJKpERAEvtlehp+rX3RKGX2yfQ0\/Vr7oisvB2HmWwzneAtkQTfut\/ORPNQecX6rfwxEnuciImtiIkqt\/6ySnaD3QSiREQQQT+jE5DaOP8AheQ\/st8h\/wDI+k3XR6h8rU5WvEybhw2y27atzFcomqZYRVK\/t6oJpn1SAg1frXC5ixaVtXDF861arVPVUchNdOaSye1b9s2J6Rb3lXbxdJjxxxuf1WtsPCFSvwWiAQRcUkBz46aztOSWOWph0p2ValFFp1KagKFKiwZVGiMBvL2G2oIHPOw0Gk1qlC7B1ZkqKCFqUjuuAdR1MvHdYFbgZZRObF6kGXx78w+iROLobcxy5F6NQcN+m1N\/tMrFSe5RJr7exzZKaFMcSqPUb7JZgAe9T3TH\/wA2T6K7LfS+5R7UWhRJsGqOClKm3945BsCPmjVjwAM4LD8nsIqqDhqLMqgb7Uk32IAG8Wte51m+lHpGo7tUqEWNSqbtbqAACouV91QBfO02\/g54soPUxz\/pNmHF6ceeTaY4jzZo0sMUzpVatI\/u3JT\/AJT71P8Ayyxw3KCpSyxKhk\/b0FPRz\/vaWZA0uykjUkKJruhBsdZEcpl6TFkjjX7Dq6dQMAykFSAQVNwQdCCNRPU4zB4psIxZATQYk1aSi5Qk3Naio9rINcyOlcP2NKorAMrAqwBUqbggi4IPES0S4mfBbFbUvUREkkkxIgkiIghIl7sr0NP1a+6JQy82XUHNUlvmaS2HYFEVl4Ow8yzP6Rfqv+KSZX7FrOyoXN284M9TZhnnp3CIk9WRETWxMtLTuN7eElj169uun87TDEjSdkRNfaOMWjSeq191ELEDU20UDiSbADrIkhScpsXzjfBFPRKhsQf3Zvu0Qet7G\/0QR8sGagmDB02ALVLGpUYvVI0321APzVAVR9FRNgCVeg6bBGKmvn5Sq3npyNBIZ+rLLOeZDQRESUkRAgHqmbEE8CPxm8Q3AmxJNxbdNzkWPUBlaV5ESFZjbLiWBItoAfZvEgey0xRJtBMeETLyfxfM1Pg7HzdUsaN9EqWLPRHYRvOo4WcZDdExTDi6G+hUMVORVhqjqQyOO0MAfCST1GGMtJr8\/Ds5M0ti4\/nqK1CLMbq6\/NqKSrrnqAwNjxFjxm5LPOzGvEkRJghEREATfwCkth+oYfPMZ3XIWvfh1fnNETcwq3OGGduYNxlaxSwvl3+3tisvB2HmXvYFTeRGsBc1bbo3QBvKBlc8LRPHJv0dO2nnOFvlLrnrESe0oiJrYiIkwCJznKitv1KWHGg8\/UGWim1FSO17uD10J0c41avOVa9b51Yot+FOj5oW7Cy1HHrJEtfRY+\/LG\/jyywYMSHfTIiTAIiIgCIiAIiSIBEXgxAEREAzcn6\/N4l6fya6c4vZVphVfPrZObIH7pp084jH1ebC1\/wBhUWre1+gLirbtNJqg8Z28mHC67H25dx8kQYksRERAJm5hXAOGJyC4c7x0Aulxc\/Zb75pTFhMZWIWwp9FVUM63NrWAv4\/f1kXydXnriiNxM7+jcU+VnydXoJkQb1SQVKkXddQSSPHW\/tieOT20CXaiyopAuvNLuqRxt7Qe28RWHNXLXuq0RLViZZM37Y9MMTLENhobWxfM0KtYC5p0ncDrKqSB4mw8Zy2Coc3TSne+4irc5kkAAkniTOl5Tf2Wr3L\/ANRZUGV26v8AnRERaWvE2JKwdPbWibEQG2vE2BBgNteJsRAba8TZbWRBG2vJtM8loJ21omxEBtrMgIIIuCLEdYOol1yYxBfC0rm7IppMTxeixpMfEoT4yvPCWPJT0Lf8TW\/6rQ253+jG6RP6s4mWTwltuQwxMsQ2GITxg9n190EU1ZWUGzMBw\/WR6h1CbIlzsv0NP1a\/hMnVYa5dbmY19G4o8q3Ymy6i1Gq1VAbdCqFNwBYD8AB16yJexF4cNcVe2p0Rp\/\/Z';
     

    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */

    public $fixtures = [
        'app.Articles',
        'app.Users',
        
    ];

   

    /**
     * Test view method
     *
     * @return void
     */
    public function testImage()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                ]
            ]
        ]);

        
                    $response = $this->get('/users');
                    $this->assertResponseOk();
                   

                    $this->assertResponseContains($this->img);


    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testJson()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                ]
            ]
        ]);

        $response = $this->get('/users.json');
        $this->assertResponseOk();
        $this->assertResponseContains(json_encode($this->img));

       
    }

   
}
